import models.Order;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.dsl.IntegrationFlow;

import java.io.File;
import java.util.List;

@ComponentScan("services")
@IntegrationComponentScan
@SpringBootApplication
public class IntegrationApp {

    @MessagingGateway
    public interface ParseAndFilterService {
        @Gateway(requestChannel = "orderFlow.input")
        List<Order> process(File file);
    }

    @Bean
    public IntegrationFlow orderFlow() {
        return flow -> flow
                .handle("CSVProcessorService", "process")
                .handle("StatusFilterService", "filter");
    }

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(IntegrationApp.class, args);
        List<Order> orders = context.getBean(ParseAndFilterService.class).process(new File("orders.csv"));
        context.close();
        orders.forEach(System.out::println);
    }
}
