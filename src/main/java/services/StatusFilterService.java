package services;

import models.Order;
import models.OrderState;
import org.springframework.stereotype.Service;

import java.util.List;

@Service(value = "StatusFilterService")
public class StatusFilterService {
    public List<Order> filter(List<Order> orders) {
        orders.removeIf(order -> order.getOrderState().equals(OrderState.CANCELED));
        return orders;
    }
}
