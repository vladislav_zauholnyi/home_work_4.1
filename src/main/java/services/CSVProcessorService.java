package services;

import com.opencsv.bean.CsvToBeanBuilder;
import lombok.extern.log4j.Log4j2;
import models.Order;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

@Log4j2
@Service(value = "CSVProcessorService")
public class CSVProcessorService {
    public List<Order> process(File file) {
        List<Order> orders;
        try {
            orders = new CsvToBeanBuilder<Order>(new FileReader(file))
                    .withType(Order.class)
                    .build()
                    .parse();
            return orders;
        } catch (FileNotFoundException e) {
            log.error(e);
        }
        return new ArrayList<>();
    }
}
