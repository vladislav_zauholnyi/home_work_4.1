package models;


import com.opencsv.bean.CsvBindByPosition;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Order {
    @CsvBindByPosition(position = 0)
    private long id;

    @CsvBindByPosition(position = 1)
    private OrderState orderState;
}
