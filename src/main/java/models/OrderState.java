package models;

public enum OrderState {
    CANCELED, WAITING_FOR_PAYMENT, PAYMENT_COMPLETED
}
